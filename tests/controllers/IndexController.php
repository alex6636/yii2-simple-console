<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   06.09.2017
 */

namespace alexs\tests\controllers;
use yii\console\Controller;
use yii\helpers\Console;

class IndexController extends Controller
{
    public function actionIndex() {
        echo $this->ansiFormat('Hello!', Console::FG_GREEN);
        return 1;
    }

    public function actionHi() {
        echo 'Hi!';
        return 1;
    }
}